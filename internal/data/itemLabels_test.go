// SPDX-FileCopyrightText: 2023 eventbike_zero Team (Verein zur Förderung innovativer Wohn- und Lebensformen e.V.) <team@eventbikezero.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package data

import (
	"testing"
)

const (
	labelString     = "a, b,c, d ; f gh i,l,  n  , o"
	labelStringSane = "a, b, c, d; f gh i, l, n, o"
)

func testLabels() []Label {
	return []Label{
		{Name: "a", Order: 4},
		{Name: "b", Order: 3},
		{Name: "c", Order: 2},
		{Name: "d", Order: 1},
		{Name: "f gh i", Order: -1},
		{Name: "l", Order: -2},
		{Name: "n", Order: -3},
		{Name: "o", Order: -4},
	}
}

func TestItemLabelsFromString(t *testing.T) {
	got := ItemLabelsFromString(labelString)
	want := testLabels()

	if len(got) != len(want) {
		t.Errorf("Unequal size: got: %+v\nwant: %+v\n", got, want)
	}
	for i := range got {
		if got[i].Name != want[i].Name || got[i].Order != want[i].Order {
			t.Errorf("\nhave: %+v\nwant: %+v\n", got, want)
		}
	}
}

func TestitemLabelsToString(t *testing.T) {
	got := itemLabelsToString(testLabels())
	if got != labelStringSane {
		t.Errorf("\nhave: %+v\nwant: %+v", got, labelStringSane)
	}
}
