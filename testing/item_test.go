// SPDX-FileCopyrightText: 2023 eventbike_zero Team (Verein zur Förderung innovativer Wohn- und Lebensformen e.V.) <team@eventbikezero.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package testing

import (
	"strings"
	"testing"
)

type Item struct {
	Object
	Org      Org
	Location Location
	Cid      string
	Comment  string
	Labels   string
}

func (i *Item) create(t *testing.T) {
	form := Form{
		Action: "/item/new",
		URL:    i.Org.Path + "/new",
		Fields: Fields{
			"cid":      []string{i.Cid},
			"labels":   []string{i.Labels},
			"location": []string{i.Location.ID},
			"orgID":    []string{i.Org.ID},
			"comment":  []string{i.Comment},
		},
		T: t,
	}

	i.Path = form.Test().
		MustStatusSeeOther().
		HTTP.Header["Location"][0]
	i.ID = strings.Split(i.Path, "/")[2]

	// only search for delimiter in markup when there are actually hidden labels
	hiddenLabelDelimiter := "" // noop
	if strings.Contains(i.Labels, ";") {
		hiddenLabelDelimiter = "</span><span class='labels l-hidden'><a "
	}

	Get(t, i.Org.Path+"/items").
		MustStatusOK().
		MustNotContain(Translate(t, "item.noAvailable")).
		MustContain(i.Cid, i.Location.Name, i.Org.Name, "<span class='labels l-active'><a href='", hiddenLabelDelimiter, i.Comment).
		MustContain(i.labels()...)
	Get(t, i.Path).
		MustStatusOK().
		MustContain(i.Cid, "<h1>"+i.Org.Name+"</h1>", ">"+i.Labels+"</textarea>", i.Comment)
}

func (i *Item) delete(t *testing.T) {
	form := Form{
		Action: "/item/delete",
		URL:    "/item/" + i.ID,
		Fields: Fields{
			"itemID": []string{i.ID},
		},
		T: t,
	}

	StringsMustEqual(t, "/", form.Test().
		MustStatusSeeOther().
		HTTP.Header["Location"][0],
	)

	_ = Get(t, i.Org.Path+"/items").
		MustStatusOK().
		MustNotContain(i.Cid)
}

// return string array from item labels
func (i *Item) labels() (labels []string) {
	labelTypes := strings.SplitN(i.Labels, ";", 2)
	labels = strings.Split(labelTypes[0], ",")
	if len(labelTypes) > 1 {
		labels = append(
			labels,
			strings.Split(labelTypes[1], ",")...,
		)
	}
	for i := range labels {
		labels[i] = strings.TrimSpace(labels[i])
	}
	return
}

func TestCanCreateItems(t *testing.T) {
	// create empty placeholders to cause a mismatch in org ids
	org := Org{Name: "Empty org"}
	org.create(t)
	org = Org{Name: "Another empty organization"}
	org.create(t)

	// create actually useful organization
	org = Org{Name: "Item Organisation"}
	org.create(t)

	location := Location{
		Org:     org,
		Name:    "Somewhere",
		Person:  "Someone",
		Comment: "'somewhere' must be an interesting place. I heard many things should have landed there over time …",
	}
	location.create(t)

	item := Item{
		Org:      org,
		Location: location,
		Cid:      "ABCD-123",
		Labels:   "label-A, label-B, LABEL-C, D, very-long-label; other-label, yet another Label, X, YZ",
		Comment:  "A test comment",
	}
	item.create(t)
}

func TestItemEdit(t *testing.T) {
	org0 := Org{Name: "Unoorganized organization"}
	org0.create(t)
	location0 := Location{
		Org:     org0,
		Name:    "Pile",
		Person:  "No one",
		Comment: "The “Pile“ is a huge stack of stuff …",
	}
	location0.create(t)
	org1 := Org{Name: "Item Edit Organisation"}
	org1.create(t)

	item1 := Item{
		Org:      org0,
		Location: location0,
		Cid:      "1",
		Labels:   "An, interesting, item; with, a, lot, of, hidden, details",
	}
	item1.create(t)

	item2 := Item{
		Org:      org0,
		Location: location0,
		Cid:      "Item Two",
		Labels:   "another, item, with, no, details",
	}
	item2.create(t)

	// Edit items
	form := Form{
		URL:    item2.Path,
		Action: item2.Path,
		T:      t,
		Fields: Fields{
			"cid":      []string{"One"},
			"labels":   []string{"an, item; but, now, the, details, note, that, the, item, is, DEFUNCT!"},
			"location": []string{"1"},
			"orgID":    []string{org1.ID},
			"comment":  []string{"Updated comment"},
		},
	}

	_ = form.Test().MustStatusSeeOther()
	_ = Get(t, form.Action).
		MustStatusOK().
		MustContain(form.Fields["cid"][0], form.Fields["comment"][0]).
		MustContain("<h1>Item Edit Organisation</h1>", form.Fields["labels"][0]+"</textarea>")
	_ = Get(t, org0.Path+"/items").
		MustStatusOK().
		MustNotContain(form.Fields["cid"][0], ">another</a>", "DEFUNCT")
	_ = Get(t, org1.Path+"/items").
		MustStatusOK().
		MustContain(form.Fields["cid"][0], ">details</a>", "DEFUNCT")
}

func TestItemDelete(t *testing.T) {
	org := Org{Name: "the pure chaos"}
	org.create(t)
	location := Location{
		Org:    org,
		Name:   "Rumpelkammer",
		Person: "??!!??",
	}
	location.create(t)

	item1 := Item{
		Org:      org,
		Location: location,
		Cid:      "ITEM-1",
		Labels:   "ding, dang, dong",
	}
	item1.create(t)
	item2 := Item{
		Org:      org,
		Location: location,
		Cid:      "ITEM-2",
		Labels:   "ping, pong, pung; ding, dong",
	}
	item2.create(t)
	item3 := Item{
		Org:      org,
		Location: location,
		Cid:      "ITEM-3",
		Labels:   "click, clack, clock, dang; ping, pong",
	}
	item3.create(t)

	// test deletion
	item3.delete(t)
	_ = Get(t, org.Path+"/items").
		MustStatusOK().
		MustContain("ITEM-1", "ITEM-2", "dang", "ping", "pong").
		MustNotContain("ITEM-3", "click", "clack", "clock")

	item1.delete(t)
	_ = Get(t, org.Path+"/items").
		MustStatusOK().
		MustContain("ITEM-2", "ping", "pong", "pung", "ding", "dong").
		MustNotContain("ITEM-3", "dang")

	item2.delete(t)
	_ = Get(t, org.Path+"/items").
		MustStatusOK().
		MustContain(Translate(t, "item.noAvailable")).
		MustNotContain("ITEM-2", "ping", "pong", "pung", "ding", "dong")

	// ensure labels are also gone from db
	var count int64
	if db.Table("labels").Where("name IN ?", []string{"ITEM-3", "dong"}).Count(&count); count > 0 {
		t.Error("A label is still left in database")
	}
}
