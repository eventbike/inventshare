// SPDX-FileCopyrightText: 2023 eventbike_zero Team (Verein zur Förderung innovativer Wohn- und Lebensformen e.V.) <team@eventbikezero.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package util

import (
	"net/http"
	"strconv"

	"eventbikezero.de/inventshare/internal/core"
	"eventbikezero.de/inventshare/internal/data"

	"github.com/go-chi/chi/v5"
)

func ItemFromPost(request *http.Request) (item data.Item, err error) {
	locationID, err := strconv.ParseUint(request.PostFormValue("location"), 10, 64)
	if err != nil {
		return
	}
	item.Location = data.Location{ID: uint(locationID)}

	item.Org = data.Org{ID: UintFromString(request.PostFormValue("orgID"))}
	if item.Org.ID == 0 {
		return item, core.UserError{Message: "Missing input: No org selected"}
	}

	item.Cid = request.PostFormValue("cid")
	item.Comment = request.PostFormValue("comment")
	item.Labels = data.ItemLabelsFromString(request.PostFormValue("labels"))

	itemID := UintFromString(chi.URLParam(request, "itemID"))
	if itemID != 0 {
		item.ID = itemID
	}

	err = item.Save()
	return
}

func LocationFromPost(request *http.Request) (data.Location, error) {
	location := data.Location{
		Name:    request.PostFormValue("name"),
		Person:  request.PostFormValue("person"),
		Comment: request.PostFormValue("comment"),
		OrgID:   UintFromString(chi.URLParam(request, "orgID")),
	}
	locationID := UintFromString(chi.URLParam(request, "locationID"))
	if locationID != 0 {
		location.ID = locationID
	}
	err := location.Save()
	return location, err
}

func OrgFromPost(request *http.Request) (data.Org, error) {
	org := data.Org{
		Name:        request.PostFormValue("name"),
		Description: request.PostFormValue("description"),
	}
	orgID := UintFromString(chi.URLParam(request, "orgID"))
	if orgID != 0 {
		org.ID = orgID
	}

	err := org.Save()
	return org, err
}
