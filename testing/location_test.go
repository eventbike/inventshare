// SPDX-FileCopyrightText: 2023 eventbike_zero Team (Verein zur Förderung innovativer Wohn- und Lebensformen e.V.) <team@eventbikezero.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package testing

import (
	"testing"
)

type Location struct {
	Object
	Org     Org
	Name    string
	Person  string
	Comment string
}

func (l *Location) create(t *testing.T) {
	// create location via HTMX
	form := Form{
		Fields: Fields{
			"name":    []string{l.Name},
			"person":  []string{l.Person},
			"comment": []string{l.Comment},
		},
		URL:    l.Org.Path + "/new",
		Action: l.Org.Path + "/location/htmx/newPost",
		T:      t,
	}

	l.ID = form.Test().
		MustStatusOK().
		MustContain("successfully saved", "<select name=\"location\" id=\"location\" required>", " selected>"+l.Name+"</option>").
		HTTP.Header["Inv-Location-Id"][0]
}
