// SPDX-FileCopyrightText: 2023 eventbike_zero Team (Verein zur Förderung innovativer Wohn- und Lebensformen e.V.) <team@eventbikezero.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package util

import (
	"html"
	"strconv"

	"eventbikezero.de/inventshare/internal/core"
	coreutil "eventbikezero.de/inventshare/internal/core/util"
	"eventbikezero.de/inventshare/internal/data"
)

// written as variadic function rather than template for performance and flexibility
// pass it the items, the translation function from the template and the fields to print as string
func RenderItems(items []data.Item, tr func(string, ...interface{}) string, fields ...string) (htm string) {
	for _, item := range items {
		htm += "<tr>"
		for _, field := range fields {
			switch field {
			case "cid":
				htm += "<td><a href='/item/" + StringFromUint(item.ID) + "'>" + html.EscapeString(item.Cid) + "</a></td>"
			case "labels":
				htm += "<td>" + renderLabels(item.Labels, tr) + "</td>"
			case "location":
				htm += "<td>" + html.EscapeString(item.Location.Name) + "</td>"
			case "comment":
				htm += "<td>" + html.EscapeString(item.Comment) + "</td>"
			default:
				htm += "<td></td>"
				core.LogMsg("Error in util.go:RenderItems, unknown field: " + field)
			}
		}
		htm += "</tr>"
	}
	if len(items) == 0 {
		htm += "<tr><td colspan='" + strconv.FormatInt(int64(len(fields))+1, 10) + "'>" + tr("item.noAvailable") + "</td></tr>"
	}
	return
}

func renderLabels(labels []data.Label, tr func(string, ...interface{}) string) (htm string) {
	n := len(labels)
	htm += "<span class='labels l-active'>"
	currentlyRenderingPrimary := true
	for i := 0; i < n; i++ {
		if labels[i].Order <= 0 && currentlyRenderingPrimary {
			htm += "</span><span class='labels l-hidden'>"
			currentlyRenderingPrimary = false
		}
		htm += "<a href='?label_id=" + StringFromUint(labels[i].ID) + "'>" + html.EscapeString(labels[i].Name) + "</a> "
	}
	htm += "</span>"
	return
}

func StringFromUint(n uint) string {
	return coreutil.StringFromUint(n)
}

func UintFromString(str string) uint {
	return coreutil.UintFromString(str)
}
