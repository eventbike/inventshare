# Automated software testing for inventshare

Tests should cover stateless functionality inside the package (e.g. string transformation),
by using "normal" Go unit tests.

However, behaviour of the software as a whole and of the routers
is tested in the `testing/` folder,
which performs several tests on the application to ensure it continues to work as new features are added.

The motivation for using this custom test suite over e.g. standard unit tests is the following:
During the first stages of the development,
most errors occured due to problems in the templates,
due to broken paths, invalid HTML, or even panics during template execution.
Covering these by unit tests would likely be more complex
than checking if the routes as seen by a user work like they should.

Behaviour that can be tested by unit tests
(e.g. correct parsing of strings and similar)
should live in normal unit test files next to the original code.

In the future, we might consider using a better framework such as Playwright.
