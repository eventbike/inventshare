// SPDX-FileCopyrightText: 2023 eventbike_zero Team (Verein zur Förderung innovativer Wohn- und Lebensformen e.V.) <team@eventbikezero.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package data

import (
	"fmt"

	"eventbikezero.de/inventshare/internal/core/settings"
	"github.com/jmoiron/sqlx"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

var (
	db  *gorm.DB
	dbx *sqlx.DB
)

var migrations = []func(*sqlx.DB){
	func(db *sqlx.DB) {
		createItemTable := "CREATE TABLE IF NOT EXISTS \"items\"  (" +
			"`id` integer PRIMARY KEY AUTOINCREMENT," +
			"`cid` text," +
			"`comment` text," +
			"`location_id` integer," +
			"`parent_id` integer," +
			"`org_id` integer," +
			"CONSTRAINT `fk_items_location` FOREIGN KEY (`location_id`) REFERENCES `locations`(`id`)," +
			"CONSTRAINT `fk_items_parent` FOREIGN KEY (`parent_id`) REFERENCES `items`(`id`)," +
			"CONSTRAINT `fk_items_org` FOREIGN KEY (`org_id`) REFERENCES `orgs`(`id`)" +
			");"
		db.MustExec(createItemTable)

		createItemLabelRelation := "CREATE TABLE IF NOT EXISTS `item_labels` (" +
			"`item_id` integer," +
			"`label_id` integer," +
			"`order` integer," +
			"PRIMARY KEY (`item_id`,`order`)," +
			"CONSTRAINT `fk_item_labels_label` FOREIGN KEY (`label_id`) REFERENCES `labels`(`id`)" +
			");"
		db.MustExec(createItemLabelRelation)

		db.MustExec("CREATE TABLE IF NOT EXISTS `labels` (`id` integer PRIMARY KEY AUTOINCREMENT,`name` text);")
	},
}

func createVersion(db *sqlx.DB) {
	q := `CREATE TABLE IF NOT EXISTS 'meta' ('key' text, 'value', integer);`
	db.MustExec(q)

	var rowexists bool
	db.Get(&rowexists, "SELECT count(key) > 0 FROM meta where key=?", "schema_version")
	if !rowexists {
		db.MustExec("INSERT INTO meta (key, value) VALUES (?, ?)", "schema_version", 0)
	}
}

func Migrate(db *sqlx.DB) {
	createVersion(db)

	var version int
	db.Get(&version, "SELECT value FROM meta WHERE key=?", "schema_version")
	if version > len(migrations) {
		fmt.Println("WARN: The current database schema version is greater than the latest known to this " +
			"version of inventshare, things may or may not work")
		return
	}
	for i, migration := range migrations[version:] {
		fmt.Printf("Apply Migration %d -> %d \n", version+i, version+i+1)
		migration(db)
		db.MustExec("UPDATE meta SET value=? WHERE key=?", version+i+1, "schema_version")
	}
}

func init() {
	var err error

	dbx = sqlx.MustConnect("sqlite3", settings.WorkDir+settings.DBFile)

	db, err = gorm.Open(sqlite.Open(settings.WorkDir+settings.DBFile), &gorm.Config{})
	if err != nil {
		panic("Failed to connect to database with error: " + err.Error())
	}

	err = db.AutoMigrate(
		&Attachment{},
		&Location{},
		&Org{},
	)
	if err != nil {
		panic("Failed to migrate with error: " + err.Error())
	}

	Migrate(dbx)
}
