// SPDX-FileCopyrightText: 2023 eventbike_zero Team (Verein zur Förderung innovativer Wohn- und Lebensformen e.V.) <team@eventbikezero.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package util

import (
	"net/http"
	"strconv"

	"github.com/go-chi/chi/v5"

	"eventbikezero.de/inventshare/internal/data"
)

func OrgFromRequest(r *http.Request) (org data.Org, err error) {
	orgID, err := strconv.ParseUint(chi.URLParam(r, "orgID"), 10, 64)
	if err != nil {
		return
	}
	org = data.Org{
		ID:   uint(orgID),
		Slug: chi.URLParam(r, "orgSlug"),
	}
	err = org.Lookup() // auto-complete missing information
	return
}

func LocationFromRequest(r *http.Request) (location data.Location, err error) {
	locationID, err := strconv.ParseUint(chi.URLParam(r, "locationID"), 10, 64)
	if err != nil {
		return
	}

	location.ID = uint(locationID)
	location.Slug = chi.URLParam(r, "locationSlug")
	err = location.Lookup() // auto-complete missing information

	return
}
