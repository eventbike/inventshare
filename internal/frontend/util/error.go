// SPDX-FileCopyrightText: 2023 eventbike_zero Team (Verein zur Förderung innovativer Wohn- und Lebensformen e.V.) <team@eventbikezero.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package util

import (
	"crypto/md5"
	"errors"
	"fmt"
	"net/http"

	"eventbikezero.de/inventshare/internal/core"

	"gorm.io/gorm"
)

func handleExecError(r Request, err error) {
	r.InitData()
	// check if is not found by database
	if errors.Is(err, gorm.ErrRecordNotFound) {
		r.ErrNotFound()
		return
	}

	errMsg := fmt.Sprintf("%s %s: %s", r.Req.Method, r.Req.URL.Path, err.Error())
	errHash := fmt.Sprintf("%x", md5.Sum([]byte(errMsg)))[0:6]
	core.LogMsg(fmt.Sprintf("[%s] %s\n%s", errHash, errMsg, core.GetCallStack()))

	r.Data["errHash"] = errHash
	r.Resp.WriteHeader(500)
	r.ExecTemplate("partials/error/generic")
}

func (r *Request) ErrNotFound() {
	r.Resp.WriteHeader(404)
	r.InitData()
	r.ExecTemplate("partials/error/notFound")
}

func ErrNotFound(w http.ResponseWriter, r *http.Request) {
	req := Request{Req: r, Resp: w}
	req.ErrNotFound()
}

func errorAfterWrite(w http.ResponseWriter, err error) {
	core.LogErr(err)
	tmpl, err := templates.GetTemplate("partials/error/templateFatal.jet.html")
	if err != nil {
		core.LogErr(err)
		return
	}
	if err = tmpl.Execute(w, nil, nil); err != nil {
		core.LogErr(err)
	}
}
