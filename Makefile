.PHONY: dev build dependencies dev-dependencies gofumpt test fmt
dev: fmt dependencies dev-dependencies
	go run .

fmt:
	gofumpt -l -w . || go fmt ./...

build: fmt dependencies
	go build .

test: fmt
	reuse lint
	rm -f -- testing/testing.db
	go test ./internal/...
	INVENTSHARE_DBFILE="testing/testing.db" INVENTSHARE_WORKDIR=$(shell pwd) GOFLAGS="-count=1" go test ./testing

dependencies: assets/css/pico-2.0.6.min.css assets/js/htmx.min.js
assets/css/pico-2.0.6.min.css:
	mkdir -p assets/css
	-rm assets/css/pico.*.css
	wget https://cdn.jsdelivr.net/npm/@picocss/pico@2.0.6/css/pico.violet.min.css -O $@
assets/js/htmx.min.js: assets/js/htmx.min.js.license
	mkdir -p assets/js
	wget https://unpkg.com/htmx.org@1.9.11 -O $@
assets/js/htmx.min.js.license: LICENSES/0BSD.txt
	echo "SPDX-License-Identifier: 0BSD" > assets/js/htmx.min.js.license
	echo "SPDX-FileCopyrightText: NONE" >> assets/js/htmx.min.js.license
LICENSES/0BSD.txt:
	wget https://github.com/bigskysoftware/htmx/raw/refs/heads/master/LICENSE -O LICENSES/0BSD.txt

dev-dependencies: gofumpt
gofumpt:
	go install mvdan.cc/gofumpt@latest || true
