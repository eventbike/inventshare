<!--
SPDX-FileCopyrightText: eventbike_zero Team (Verein zur Förderung innovativer Wohn- und Lebensformen e.V.) <team@eventbikezero.de>

SPDX-License-Identifier: CC0-1.0
-->

# Code style guide

When contributing code to inventshare, the following basic principles should be kept in mind:

- Use descriptive names for functions and variables:
	- variables: e.g. use `locationIDs` instead of `locids` or `keys`
	- functions: e.g. use `itemsLocationsFillAll()` instead of `iLocFill()`
- Avoid long functions and split into small functions with descriptive names as much as possible:
	- Even if it does not immediately benefit the software with deduplication,
		this can help debugging and reading code by a lot.
	- You can use functions to avoid comments in the code, e.g. use
	
	~~~go
	prefillData(&slice)
	~~~
	
	instead of
	
	~~~go
	// prefill data
	... actual code
	~~~


## The `data` package

- methods should not alter the original object
	- exceptions are .Save(), .Create(), .Lookup()
- .Lookup(): return one object matching existing criteria
- to access data which is not contained in the struct, a method with the corresponding name (as if it would have been a field) is created
- if a function is not a method of an existing struct, it should be prefixed with the related struct name / filename
	- example: Struct.Foo() → StructFoo(x)
	- or for already combined structs (join tables): FooBar.Meth() → FooBarMeth()
- Filters:
	- <Foo>sFilter() finds multiple objects
	- an empty filter query acts as a list of items
- functions returning lists should use plural naming, examples:
	- BarsFilter (returns multiple Bars from Bar),
	- Bar.Foos (returns multiple Foos)

### File structure

The files should always be structured with the following sections:

~~~
- Header, package, import
- type definitions

// Primary Operations

Save() (, Create())
Delete()

// Finders

→ find this object in the database (from id or search)
Lookup()
xByID()

// External

→ get external data types that match the struct

// Helpers

→ other methods like slug etc

~~~
