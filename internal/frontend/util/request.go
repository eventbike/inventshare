// SPDX-FileCopyrightText: 2023 eventbike_zero Team (Verein zur Förderung innovativer Wohn- und Lebensformen e.V.) <team@eventbikezero.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package util

import (
	"net/http"

	"eventbikezero.de/inventshare/internal/core/settings"
	"eventbikezero.de/inventshare/internal/data"
	"github.com/kataras/i18n"
)

type Request struct {
	Resp   http.ResponseWriter
	Req    *http.Request
	Data   map[string]interface{}
	locale *i18n.Locale
}

func (r *Request) InitData() {
	if r.Data == nil {
		r.Data = make(map[string]interface{})
	}
	r.initTranslations()

	// provide functions
	r.Data["tr"] = r.Translate
	r.Data["AppPath"] = settings.AppPath
	r.Data["empty"] = data.Empty
}

func (r *Request) ExecTemplate(name string) {
	tmpl, err := templates.GetTemplate(name + ".jet.html")
	if err != nil {
		errorAfterWrite(r.Resp, err)
		return
	}
	if err = tmpl.Execute(r.Resp, nil, r.Data); err != nil {
		errorAfterWrite(r.Resp, err)
	}
}

func (r *Request) Redirect(path string, status int) {
	http.Redirect(r.Resp, r.Req, path, status)
}
