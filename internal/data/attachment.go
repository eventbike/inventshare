// SPDX-FileCopyrightText: 2023 eventbike_zero Team (Verein zur Förderung innovativer Wohn- und Lebensformen e.V.) <team@eventbikezero.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package data

import (
	"io/ioutil"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"time"

	"gorm.io/gorm"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

type Attachment struct {
	gorm.Model
	Filename string
	FileKey  string
	Size     int64
	Mime     string
}

func AttachmentNew(attachment *Attachment) error {
	result := db.Create(&attachment)
	if result.Error != nil {
		return result.Error
	}
	return nil
}

func AttachmentFromForm(request *http.Request, formName string) (Attachment, error) {
	formAttachment, handler, err := request.FormFile(formName)
	if err != nil {
		return Attachment{}, err
	}
	defer formAttachment.Close()

	attachment := Attachment{
		Filename: handler.Filename,
		FileKey:  AttachmentRandomString(12),
		Size:     handler.Size,
		Mime:     handler.Header.Get("Content-Type"),
	}
	if err = AttachmentNew(&attachment); err != nil {
		return attachment, err
	}

	attachmentBytes, err := ioutil.ReadAll(formAttachment)
	if err != nil {
		return attachment, err
	}

	err = os.WriteFile("data/attachment/"+strconv.FormatUint(uint64(attachment.ID), 10)+"-"+attachment.FileKey+"-"+attachment.Filename, attachmentBytes, 0o600)
	if err != nil {
		return attachment, err
	}
	return attachment, nil
}

func AttachmentRandomString(letters int) string {
	alphabet := []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

	str := make([]rune, letters)
	for i := range str {
		str[i] = alphabet[rand.Intn(len(alphabet))]
	}
	return string(str)
}
