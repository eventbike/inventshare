// SPDX-FileCopyrightText: 2023 eventbike_zero Team (Verein zur Förderung innovativer Wohn- und Lebensformen e.V.) <team@eventbikezero.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package data

type Label struct {
	ID   uint
	Name string

	// should avoid creating the field in database, but allows reading the value from custom SQL statements
	Order int `gorm:"-:migration"`
}

// Primary Operations

func (label *Label) Create() error {
	return db.Create(&label).Error
}

func (label *Label) Delete() error {
	return db.Delete(&label).Error
}
