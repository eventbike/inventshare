// SPDX-FileCopyrightText: 2023 eventbike_zero Team (Verein zur Förderung innovativer Wohn- und Lebensformen e.V.) <team@eventbikezero.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package org

import (
	"net/http"

	"eventbikezero.de/inventshare/internal/core/settings"
	"eventbikezero.de/inventshare/internal/data"
	item_frontend "eventbikezero.de/inventshare/internal/frontend/item"
	"eventbikezero.de/inventshare/internal/frontend/util"

	"github.com/go-chi/chi/v5"
)

func Router(router chi.Router) {
	router.Get("/", util.ExecFrontend(index, "org/index"))
	router.Get("/new", util.ExecFrontend(newOrgForm, "org/new"))
	router.Post("/new", util.Exec(saveOrgPost))
	router.Post("/htmx/new", util.ExecFrontend(htmxNewOrgPost, "partials/select/org"))
	router.Post("/delete", util.Exec(orgDeletePost))
	router.Route("/{orgID}-{orgSlug}", func(subrouter chi.Router) {
		subrouter.Get("/", util.ExecFrontend(orgSummary, "org/summary"))
		subrouter.Get("/new", util.ExecFrontend(newItemForm, "org/newItem"))
		subrouter.Get("/duplicate/{itemID}", util.ExecFrontend(cloneItemForm, "org/newItem"))
		subrouter.Get("/items", util.ExecFrontend(orgItems, "org/items"))
		subrouter.Get("/settings", util.ExecFrontend(orgSettingsForm, "org/edit"))
		subrouter.Post("/settings", util.Exec(saveOrgPost))
		subrouter.Route("/location", locationRouter)
		subrouter.Get("/locations", util.ExecFrontend(locationIndex, "org/location/index"))
	})
}

func index(r util.Request) (err error) {
	r.Data["Orgs"], err = data.OrgList()
	return
}

func newOrgForm(r util.Request) error {
	r.Data["Org"] = data.Org{}
	return nil
}

func orgSettingsForm(r util.Request) (err error) {
	org, err := util.OrgFromRequest(r.Req)
	r.Data["Org"] = &org
	return
}

func saveOrgPost(r util.Request) error {
	org, err := util.OrgFromPost(r.Req)
	if err != nil {
		return err
	}

	r.Redirect(org.Path(), http.StatusSeeOther)
	return nil
}

func htmxNewOrgPost(r util.Request) (err error) {
	orgs, err := data.OrgList()
	if err != nil {
		return
	}
	r.Data["Orgs"] = &orgs

	org, err := util.OrgFromPost(r.Req)
	if err != nil {
		return
	}

	r.Data["SelectedOrg"] = &org
	r.Data["SuccessMessage"] = true

	return
}

func orgDeletePost(r util.Request) error {
	org := data.Org{
		ID: util.UintFromString(r.Req.PostFormValue("orgID")),
	}
	if err := org.Delete(); err != nil {
		return err
	}
	r.Redirect(settings.AppPath, http.StatusSeeOther)
	return nil
}

func orgItems(r util.Request) (err error) {
	r.Data["RenderItems"] = util.RenderItems

	org, err := util.OrgFromRequest(r.Req)
	if err != nil {
		return
	}
	r.Data["Org"] = &org
	r.Data["Items"], err = org.Items()
	return
}

func orgSummary(r util.Request) (err error) {
	org, err := util.OrgFromRequest(r.Req)
	if err != nil {
		return
	}
	r.Data["Org"] = &org
	r.Data["Stats"] = org.Stats()

	return
}

func newItemForm(r util.Request) (err error) {
	org, err := util.OrgFromRequest(r.Req)
	if err != nil {
		return
	}
	item := data.Item{
		Org: org,
	}

	return item_frontend.ItemFormPrepare(&r, &item)
}

func cloneItemForm(r util.Request) (err error) {
	item, err := data.ItemByID(util.UintFromString(chi.URLParam(r.Req, "itemID")))
	if err != nil {
		return
	}
	item.ID = 0

	return item_frontend.ItemFormPrepare(&r, &item)
}
