# Contributing to inventshare

Thank you for considering a contribution to inventshare.
We need your support in every domain
– read on to learn how you can help.


## Using inventshare

We appreciate if you want to use inventshare.
The software is currently in an early state,
and self-hosting is not recommended without contacting us.

To elaborate, the software will see breaking changes,
and while we are not aware of significant users,
we will not deal with them in a graceful way.

If you intend to use inventshare,
please let us know and we'll tell you what you need to know.

Also, we are actively conducting User Research and User Testing,
so we'll likely reach out to learn from how you interact with the software
by scheduling sessions and observing your behaviour.


## Financial contribution: Fund the development

inventshare is developed by the eventbike_zero,
a local event collective from Potsdam, Germany.
We accept donations, see https://eventbikezero.de/spenden/ (German).

If you have a special funding opportunity,
please contact us at inventshare@eventbikezero.de.

We are available for sponsored development:
If your organization or company requires a certain feature in inventshare,
we are ready to talk about implementing it for a certain fee.


## Translating

If you want to translate inventshare to a specific language,
please reach out to get access to the Weblate project.
Translations are currently locked,
because we see no point in having partial translations without an active user base in many languages.


## Design and Management

inventshare has a high interest in UX design and managing meta tasks around the project
(like doing user research and polls, improving in-app hints and documentation,
checking the software for UX problems and defining priorities for the development).

If this sounds good to you,
please let us know and join the team!

Also have a look at the dedicated [User_Experience.md](./User_Experience.md) file.


## Development

If you would like to help, please let us know.
There is some documentation available in this folder,
but getting in touch is probably the best idea,
because things are in a flux.

We'll happily start writing onboarding articles if there is interest
in participating in the development.
