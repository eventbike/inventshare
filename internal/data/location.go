// SPDX-FileCopyrightText: 2023 eventbike_zero Team (Verein zur Förderung innovativer Wohn- und Lebensformen e.V.) <team@eventbikezero.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package data

import (
	"eventbikezero.de/inventshare/internal/core/util"
	"github.com/jmoiron/sqlx"
)

type Location struct {
	ID      uint
	Name    string
	Person  string
	Comment string
	Slug    string
	Org     Org
	OrgID   uint
}

// Primary Operations

func (location *Location) Save() error {
	location.createSlug()
	return db.Save(&location).Error
}

func (location *Location) Delete() error {
	return db.Delete(&location).Error
}

// Finders

func (location *Location) Lookup() error {
	return db.Where(location).Preload("Org").Take(location).Error
}

func LocationsFromIDs(locationIDs []uint) (locationMap map[uint]Location, err error) {
	// query needed locations
	query, args, err := sqlx.In(
		"SELECT id, name, person, comment, slug, org_id FROM locations WHERE id in (?);",
		locationIDs)
	if err != nil {
		return
	}

	locationMap = make(map[uint]Location)
	rows, err := dbx.Queryx(query, args...)
	for rows.Next() {
		var id, org_id uint
		var name, person, comment, slug string

		rows.Scan(&id, &name, &person, &comment, &slug, &org_id)
		locationMap[id] = Location{id, name, person, comment, slug, Org{}, org_id}
	}
	return
}

// External

func (location *Location) Items() (items []Item, err error) {
	err = dbx.Select(&items, `SELECT id, cid, comment, location_id as locationid FROM items WHERE location_id = ?`, location.ID)
	if err != nil {
		return
	}
	itemsLocationsFillAll(&items)
	itemLabelsFillAll(&items)
	return
}

// Helpers

func (location *Location) Path() string {
	return location.Org.Path() + "/location/" + util.StringFromUint(location.ID) + "-" + location.Slug
}

func (location *Location) createSlug() {
	location.Slug = createSlug(location.Name)
}
