// SPDX-FileCopyrightText: 2023 eventbike_zero Team (Verein zur Förderung innovativer Wohn- und Lebensformen e.V.) <team@eventbikezero.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package data

import (
	"regexp"
	"strings"
)

// Provides empty types, mostly used in templates when no existing data is available
func Empty(kind string) interface{} {
	switch kind {
	case "location":
		return Location{}
	}
	return nil
}

// turns user input into URL slugs
func createSlug(input string) string {
	input = strings.ToLower(input)
	input = strings.ReplaceAll(input, " ", "-")
	input = strings.NewReplacer(
		"ß", "ss",
		"ä", "ae",
		"ö", "oe",
		"ü", "ue",
		"Ä", "ae",
		"Ö", "oe",
		"Ü", "ue").Replace(input)
	input = regexp.MustCompile("[^a-z0-9-_]").ReplaceAllString(input, "-") // remove all unsupported characters and replace with "-"
	input = regexp.MustCompile("-+").ReplaceAllString(input, "-")          // replace multiple consecutive dashes, e.g. "--"
	input = strings.Trim(input, "-")
	return input
}
