// SPDX-FileCopyrightText: 2023 eventbike_zero Team (Verein zur Förderung innovativer Wohn- und Lebensformen e.V.) <team@eventbikezero.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package util

import (
	"eventbikezero.de/inventshare/assets"
	"eventbikezero.de/inventshare/internal/core"
)

func (r *Request) initTranslations() {
	// auto-detect language from request and save for Translate function
	r.locale = assets.I18n.GetLocale(r.Req)
}

func (r *Request) Translate(key string, args ...interface{}) (msg string) {
	msg = r.locale.GetMessage(key, args)
	if msg != "" {
		return
	}
	core.LogMsg("Translation: Key " + key + " not found in locale " + r.locale.ID)
	msg = assets.I18n.Tr("en", key, args)
	if msg == "" {
		msg = key
	}
	return
}
