// SPDX-FileCopyrightText: 2023 eventbike_zero Team (Verein zur Förderung innovativer Wohn- und Lebensformen e.V.) <team@eventbikezero.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package util

import (
	"net/http"

	"eventbikezero.de/inventshare/internal/core/settings"
	"github.com/CloudyKit/jet/v6"
)

var templates = jet.NewSet(
	jet.NewOSFileSystemLoader(settings.WorkDir+"templates"),
	jet.InDevelopmentMode(), // to be made configurable before production
)

/* Exec allows to wrap around routing functions,
 * but gracefully logs errors that are returned from them.
 * Use ExecFrontend instead if you want to execute templates.
 */
func Exec(fn func(r Request) error) func(w http.ResponseWriter, r *http.Request) {
	// returns a function which matches the signature expected by the routers
	return func(w http.ResponseWriter, req *http.Request) {
		r := Request{
			Resp: w,
			Req:  req,
		}
		if err := fn(r); err != nil {
			handleExecError(r, err)
		}
	}
}

// Helper for frontend routes to reduce boilerplate
// automatically initializes data and executes template
func ExecFrontend(fn func(r Request) error, templateName string) func(w http.ResponseWriter, r *http.Request) {
	wrappedFunc := func(r Request) (err error) {
		r.InitData()
		if err = fn(r); err == nil {
			r.ExecTemplate(templateName)
		}
		return err
	}
	return Exec(wrappedFunc)
}
