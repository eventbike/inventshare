// SPDX-FileCopyrightText: 2023 eventbike_zero Team (Verein zur Förderung innovativer Wohn- und Lebensformen e.V.) <team@eventbikezero.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package core

import (
	"fmt"
	"os"
	"runtime"
)

type UserError struct {
	Message string
	Err     error
}

func (e UserError) Error() string {
	return e.Message
}

type InternalError struct {
	Message string
	Err     error
}

func (e InternalError) Error() string {
	return e.Message
}

func LogMsg(message string) {
	fmt.Fprintln(os.Stderr, message)
}

func LogErr(err error) {
	if err != nil {
		LogMsg(err.Error())
	}
}

func LogMsgReturn(message string) error {
	LogMsg(message)
	return InternalError{Message: message}
}

func LogErrReturn(err error) error {
	LogErr(err)
	return err
}

func GetCallStack() (callers string) {
	skip := 3
	for _, file, no, ok := runtime.Caller(skip); ok; _, file, no, ok = runtime.Caller(skip) {
		skip++
		callers += fmt.Sprintf(" - %s:L%d\n", file, no)
	}
	return
}
