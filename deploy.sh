#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2023 eventbike_zero Team (Verein zur Förderung innovativer Wohn- und Lebensformen e.V.) <team@eventbikezero.de>
#
# SPDX-License-Identifier: CC0-1.0

make build
rsync -aP --delete --progress -e "ssh -i ../syncthing/secrets/id_eventbike" inventshare templates "team@eventbikezero.de:inventshare/"
ssh -i ../syncthing/secrets/id_eventbike team@eventbikezero.de 'killall inventshare'
