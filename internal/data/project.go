// SPDX-FileCopyrightText: 2023 eventbike_zero Team (Verein zur Förderung innovativer Wohn- und Lebensformen e.V.) <team@eventbikezero.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package data

import (
	"time"

	"gorm.io/gorm"
)

type Project struct {
	gorm.Model
	Name      string
	Startdate time.Time
	Enddate   time.Time
	Items     []Item `gorm:"many2many:project_items;"`
}

func ProjectNew(project *Project) error {
	result := db.Create(&project)
	if result.Error != nil {
		return result.Error
	}
	return nil
}

func ProjectList() ([]Project, error) {
	var projects []Project
	result := db.Where("startdate >= ?", time.Now().Format("2006-01-02")).Order("startdate").Find(&projects)
	if result.Error != nil {
		return nil, result.Error
	}
	return projects, nil
}

func GetProject(ID uint) (Project, error) {
	var project Project
	db.Preload("Items.Labels").First(&project, ID)
	return project, nil
}
