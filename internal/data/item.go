// SPDX-FileCopyrightText: 2023 eventbike_zero Team (Verein zur Förderung innovativer Wohn- und Lebensformen e.V.) <team@eventbikezero.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package data

import (
	"eventbikezero.de/inventshare/internal/core/settings"
	"eventbikezero.de/inventshare/internal/core/util"
)

type Item struct {
	ID         uint
	Cid        string
	Comment    string
	Location   Location
	LocationID uint
	Parent     *Item   `gorm:"default:NULL"`
	Labels     []Label `gorm:"many2many:item_labels;"`
	Org        Org
	Projects   []Project `gorm:"many2many:project_items;"`
}

// Primary Operations

func (item *Item) Save() error {
	if item.ID > 0 {
		_, err := dbx.Exec("UPDATE items SET cid=?, comment=?, location_id=?, org_id=? WHERE id=?;",
			item.Cid, item.Comment, item.Location.ID, item.Org.ID, item.ID)
		if err != nil {
			return err
		}
	} else {
		res, err := dbx.Exec("INSERT INTO items (cid, comment, location_id, org_id) VALUES (?, ?, ?, ?)",
			item.Cid, item.Comment, item.Location.ID, item.Org.ID)
		if err != nil {
			return err
		}
		id, err := res.LastInsertId()
		if err != nil {
			return err
		}
		item.ID = uint(id)
	}
	return item.LabelsSync()
}

func (item *Item) Delete() error {
	_, err := dbx.Exec("DELETE FROM items WHERE id=?", item.ID)
	itemLabelsCleanup() // make sure that leftover item labels are cleaned
	return err
}

// Finders

func (item *Item) Lookup() (Item, error) {
	err := db.Where(item).Preload("Location").Preload("Org").First(item).Error
	if err != nil {
		return *item, err
	}
	return *item, item.labelsFill()
}

func ItemFilter(item *Item) (items []Item, err error) {
	err = db.Where(item).Preload("Location").Find(&items).Error
	if err != nil {
		return
	}
	itemLabelsFillAll(&items)
	return
}

func ItemByID(itemID uint) (item Item, err error) {
	err = dbx.QueryRow(`SELECT 
			i.id, i.cid, i.comment, 
			l.id, l.name, l.person, l.comment, l.slug,
			o.id, o.name, o.slug, o.description
		FROM items as i
			LEFT JOIN locations AS l ON i.location_id = l.id
			LEFT JOIN orgs AS o ON i.org_id = o.id
		WHERE i.id = ?`, itemID).Scan(&item.ID, &item.Cid, &item.Comment,
		&item.Location.ID, &item.Location.Name, &item.Location.Person, &item.Location.Comment, &item.Location.Slug,
		&item.Org.ID, &item.Org.Name, &item.Org.Slug, &item.Org.Description)

	item.LocationID = item.Location.ID
	item.Location.Org = item.Org

	if err != nil {
		return
	}
	err = item.labelsFill()
	return
}

func itemsLocationsFillAll(items *[]Item) error {
	locationIDs := locationIDsFromItems(items)
	locations, err := LocationsFromIDs(locationIDs)
	if err != nil {
		return err
	}

	// assign locations to items
	for itemID := range *items {
		(*items)[itemID].Location = locations[(*items)[itemID].LocationID]
	}
	return nil
}

// Helpers

func (item *Item) Path() string {
	return settings.AppPath + "item/" + util.StringFromUint(item.ID)
}

func (item *Item) LabelString() (str string) {
	return itemLabelsToString(item.Labels)
}

func locationIDsFromItems(items *[]Item) (locationIDs []uint) {
	locationIDmap := make(map[uint]bool)
	for _, item := range *items {
		locationIDmap[item.LocationID] = true
	}
	for v := range locationIDmap {
		locationIDs = append(locationIDs, v)
	}
	return
}
