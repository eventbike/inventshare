// SPDX-FileCopyrightText: 2023 eventbike_zero Team (Verein zur Förderung innovativer Wohn- und Lebensformen e.V.) <team@eventbikezero.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package data

import (
	"eventbikezero.de/inventshare/internal/core/settings"
	"eventbikezero.de/inventshare/internal/core/util"
)

type Org struct {
	ID          uint
	Name        string
	Description string
	Slug        string
}

type OrgStats struct {
	Items     uint64
	Locations uint64
}

// Primary Operations

func (org *Org) Save() error {
	org.Slug = org.createSlug()
	return db.Save(&org).Error
}

func (org *Org) Delete() error {
	return db.Delete(&org).Error
}

// Finders

func (org *Org) Lookup() error {
	return db.Where(&org).Take(&org).Error
}

func OrgByID(id uint) (org Org, err error) {
	err = db.Where("org_id = ?", id).Take(&org).Error
	return
}

func OrgList() (orgs []Org, err error) {
	err = db.Find(&orgs).Error
	return
}

// References

func (org *Org) Items() (items []Item, err error) {
	err = dbx.Select(&items, `SELECT id, cid, comment, location_id as locationid FROM items WHERE org_id = ?`, org.ID)
	if err != nil {
		return
	}
	itemsLocationsFillAll(&items)
	itemLabelsFillAll(&items)
	return
}

func (org *Org) Locations() (locations []Location, err error) {
	err = db.Where("org_id = ?", org.ID).Find(&locations).Error
	return
}

// Helpers

func (org *Org) Path() string {
	return settings.AppPath + "org/" + util.StringFromUint(org.ID) + "-" + org.Slug
}

func (org *Org) Stats() (stats OrgStats) {
	dbx.Get(&stats.Items, "SELECT COUNT(id) FROM items WHERE org_id=?", org.ID)
	dbx.Get(&stats.Locations, "SELECT COUNT(id) FROM locations WHERE org_id=?", org.ID)
	return
}

func (org *Org) createSlug() string {
	return createSlug(org.Name)
}
