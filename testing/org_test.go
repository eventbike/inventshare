// SPDX-FileCopyrightText: 2023 eventbike_zero Team (Verein zur Förderung innovativer Wohn- und Lebensformen e.V.) <team@eventbikezero.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package testing

import (
	"fmt"
	"strings"
	"testing"
)

type Org struct {
	Object
	Name        string
	Description string
}

func dummyOrg(t *testing.T) Org {
	org := Org{
		Name:        dummyNext() + "Generic reusable dummy orgnaization",
		Description: "with some description …",
	}
	org.create(t)
	return org
}

func (o *Org) create(t *testing.T) {
	form := Form{
		Fields: Fields{
			"name":        []string{o.Name},
			"description": []string{o.Description},
		},
		URL:    "/org/new",
		Action: "/org/new",
		T:      t,
	}

	o.Path = form.Test().
		MustStatusSeeOther().
		MustEmpty().
		HTTP.Header["Location"][0] // Location field contains redirected location
	o.ID = strings.Split(strings.Split(o.Path, "/")[2], "-")[0]

	o.verify(t)
	// verify items are empty
	_ = Get(t, o.Path+"/items").
		MustStatusOK().
		MustContain(Translate(t, "item.noAvailable"))
}

func (o *Org) verify(t *testing.T) {
	Get(t, o.Path).
		MustStatusOK().
		MustContain(o.Name, o.Description)
}

func (o *Org) edit(t *testing.T) {
	form := Form{
		Fields: Fields{
			"name":        []string{o.Name},
			"description": []string{o.Description},
		},
		URL:    o.Path + "/settings",
		Action: o.Path + "/settings",
		T:      t,
	}

	o.Path = form.Test().
		MustStatusSeeOther().
		MustEmpty().
		HTTP.Header["Location"][0]

	o.verify(t)
}

func (o *Org) delete(t *testing.T) {
	form := Form{
		Action: "/org/delete",
		URL:    o.Path + "/settings",
		Fields: Fields{
			"orgID": []string{o.ID},
		},
		T: t,
	}

	StringsMustEqual(t, "/", form.Test().
		MustStatusSeeOther().
		HTTP.Header["Location"][0],
	)

	_ = Get(t, o.Path).
		MustStatusNotFound()

	_ = Get(t, "/org").
		MustStatusOK().
		MustNotContain(o.Name)
}

func TestCanCreateOrg(t *testing.T) {
	(&Org{
		Name:        "inventshare Demo-Organization",
		Description: "The first inventshare Demo-Organization",
	}).create(t)
}

func TestNotFoundOnDifferentSlug(t *testing.T) {
	org := Org{
		Name:        "My Difficult Slug!",
		Description: "",
	}
	org.create(t)
	if !strings.Contains(org.Path, "-my-difficult-slug") {
		t.Error(fmt.Errorf("orgURL did not match expected slug: %s", org.Path))
	}
	_ = Get(t, org.Path+"-changed-url").
		MustStatusNotFound()
	_ = Get(t, org.Path[0:8]).
		MustStatusNotFound()
}

func TestEditOrgs(t *testing.T) {
	org := Org{
		Name:        "Working title",
		Description: "Not quite ready yet",
	}
	org.create(t)

	org.Name = "Now called X"
	org.Description = "This is the final name"
	org.edit(t)
}
