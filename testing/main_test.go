// SPDX-FileCopyrightText: 2023 eventbike_zero Team (Verein zur Förderung innovativer Wohn- und Lebensformen e.V.) <team@eventbikezero.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package testing

import (
	"context"
	"net/http"
	"testing"
	"time"

	"eventbikezero.de/inventshare/internal/core/settings"
	"eventbikezero.de/inventshare/internal/frontend"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

const bindString = "127.0.0.1:3993"

var db *gorm.DB

func connectToDB() {
	var err error
	db, err = gorm.Open(sqlite.Open(settings.WorkDir+settings.DBFile), &gorm.Config{})
	if err != nil {
		panic("Failed to connect to database with error: " + err.Error())
	}
}

func TestMain(m *testing.M) {
	// create a context that allows cancelling the server when all tests are done
	ctx, cancelServer := context.WithCancel(context.Background())

	// import the router from the existing project
	router := frontend.Router()
	// and run it in parallel ("go" starts a function in background context here)
	go func(ctx context.Context) {
		if err := http.ListenAndServe(bindString, router); err != nil {
			panic(err.Error())
		}
	}(ctx)

	// wait until server is up
	for connectionAttempts := 0; connectionAttempts < 5; connectionAttempts++ {
		if _, err := http.Get("http://" + bindString); err == nil {
			break
		}
		time.Sleep(time.Second)
	}

	connectToDB()
	// run tests
	m.Run()
	cancelServer() // shutdown the webserver running in background via the provided context function
}

func TestAssetsAvailable(t *testing.T) {
	_ = Get(t, "/static/css/inventshare.css").
		MustStatusOK().
		MustNotEmpty()
	_ = Get(t, "/static/css/pico-2.0.6.min.css").
		MustStatusOK().
		MustNotEmpty()
	_ = Get(t, "/static/js/htmx.min.js").
		MustStatus(200). // response contains dangerous keywords, so MustStatusOK fails
		MustNotEmpty()
}
