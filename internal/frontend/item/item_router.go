// SPDX-FileCopyrightText: 2023 eventbike_zero Team (Verein zur Förderung innovativer Wohn- und Lebensformen e.V.) <team@eventbikezero.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package item

import (
	"net/http"

	"eventbikezero.de/inventshare/internal/core/settings"
	"eventbikezero.de/inventshare/internal/data"
	"eventbikezero.de/inventshare/internal/frontend/util"

	"github.com/go-chi/chi/v5"
)

func Router(router chi.Router) {
	// router.Get("/new", util.LogErrors("item: NewItemForm", NewItemForm))
	router.Get("/{itemID}", util.ExecFrontend(editForm, "item/edit"))
	router.Post("/{itemID}", util.Exec(savePost))
	router.Post("/new/", util.Exec(savePost))
	router.Post("/delete", util.Exec(delete))
}

func editForm(r util.Request) (err error) {
	item, err := data.ItemByID(util.UintFromString(chi.URLParam(r.Req, "itemID")))
	if err != nil {
		return
	}
	r.Data["Org"] = &item.Org
	r.Data["SelectedLocation"] = item.Location.ID

	return ItemFormPrepare(&r, &item)
}

func ItemFormPrepare(r *util.Request, item *data.Item) (err error) {
	r.Data["Item"] = &item
	r.Data["AvailableLocations"], err = item.Org.Locations()
	return
}

func savePost(r util.Request) error {
	item, err := util.ItemFromPost(r.Req)
	if err != nil {
		return err
	}

	r.Redirect("/item/"+util.StringFromUint(item.ID), http.StatusSeeOther)
	return nil
}

func delete(r util.Request) error {
	item := data.Item{
		ID: util.UintFromString(r.Req.PostFormValue("itemID")),
	}
	if err := item.Delete(); err != nil {
		return err
	}
	r.Redirect(settings.AppPath, http.StatusSeeOther) // ToDo: Return to previous page
	return nil
}
