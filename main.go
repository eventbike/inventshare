// SPDX-FileCopyrightText: 2023 eventbike_zero Team (Verein zur Förderung innovativer Wohn- und Lebensformen e.V.) <team@eventbikezero.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package main

import (
	"fmt"
	"net/http"

	"eventbikezero.de/inventshare/internal/core/settings"
	"eventbikezero.de/inventshare/internal/frontend"
)

func main() {
	router := frontend.Router()
	fmt.Println("Starting to listen on http://" + settings.Addr + ":" + settings.Port)
	if err := http.ListenAndServe(settings.Addr+":"+settings.Port, router); err != nil {
		fmt.Printf(err.Error())
	}
}
