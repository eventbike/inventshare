// SPDX-FileCopyrightText: 2023 eventbike_zero Team (Verein zur Förderung innovativer Wohn- und Lebensformen e.V.) <team@eventbikezero.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package settings

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

var (
	Port    string
	Addr    string
	DBFile  string
	WorkDir string
	AppPath string
)

func init() {
	if Port = os.Getenv("INVENTSHARE_PORT"); Port == "" {
		Port = "4000"
	}
	if Addr = os.Getenv("INVENTSHARE_ADDR"); Addr == "" {
		Addr = "127.0.0.1"
	}
	if DBFile = os.Getenv("INVENTSHARE_DBFILE"); DBFile == "" {
		DBFile = "inventshare.db"
	}
	if WorkDir = os.Getenv("INVENTSHARE_WORKDIR"); WorkDir == "" {
		setWorkDir()
	}
	if !strings.HasSuffix(WorkDir, "/") {
		WorkDir += "/"
	}
	if AppPath = os.Getenv("INVENTSHARE_APPPATH"); AppPath == "" {
		AppPath = "/"
	}
}

func setupFailed(message string, err error) {
	panic("Setup failed: " + message + ": " + err.Error())
}

func setWorkDir() {
	var err error
	// try existing workdir
	WorkDir, err = os.Getwd()
	if err != nil {
		setupFailed("Unable to get current workDir", err)
	}
	fmt.Println(WorkDir)
	if _, err = os.Stat(WorkDir + "/templates"); err == nil { // check if templates are present
		return
	}

	// else set path of executable
	executablePath, err := os.Executable()
	if err != nil {
		setupFailed("Unable to determine executable path", err)
	}

	WorkDir = filepath.Dir(executablePath)
	fmt.Println("Setup info: WorkDir set to " + WorkDir)
	if err = os.Chdir(WorkDir); err != nil {
		setupFailed("unable to change WorkDir", err)
	}
}
