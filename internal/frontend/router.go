// SPDX-FileCopyrightText: 2023 eventbike_zero Team (Verein zur Förderung innovativer Wohn- und Lebensformen e.V.) <team@eventbikezero.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package frontend

import (
	"net/http"
	"os"
	"path/filepath"
	"strings"

	//"eventbikezero.de/inventshare/internal/frontend/adhoc"
	"eventbikezero.de/inventshare/assets"
	"eventbikezero.de/inventshare/internal/frontend/item"
	"eventbikezero.de/inventshare/internal/frontend/org"
	"eventbikezero.de/inventshare/internal/frontend/util"

	"github.com/go-chi/chi/v5"
)

var staticAssets http.Handler

func init() {
	staticAssets = http.StripPrefix("/static/", http.FileServer(http.FS(assets.Static)))
}

func Router() chi.Router {
	router := chi.NewRouter()

	// router.Get("/", landingPage)
	router.Route("/item", item.Router)
	// router.Route("/adhoc", adhoc.Router)
	router.Route("/org", org.Router)

	router.NotFound(util.ErrNotFound)
	router.Get("/static/*", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Cache-Control", "max-age=7200")
		staticAssets.ServeHTTP(w, r)
	})
	router.Get("/data/attachment/*", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Cache-Control", "max-age=7200; private")
		workDir, _ := os.Getwd()
		filesDir := http.Dir(filepath.Join(workDir, "data/attachment"))
		rctx := chi.RouteContext(r.Context())
		pathPrefix := strings.TrimSuffix(rctx.RoutePattern(), "/*")
		fs := http.StripPrefix(pathPrefix, http.FileServer(filesDir))
		fs.ServeHTTP(w, r)
	})

	return router
}
