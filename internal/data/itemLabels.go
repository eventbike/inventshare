// SPDX-FileCopyrightText: 2023 eventbike_zero Team (Verein zur Förderung innovativer Wohn- und Lebensformen e.V.) <team@eventbikezero.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package data

import (
	"errors"
	"fmt"
	"strings"

	"eventbikezero.de/inventshare/internal/core"
	"gorm.io/gorm"
)

type ItemLabels struct {
	ItemID  uint `gorm:"primaryKey"`
	Label   Label
	LabelID uint
	Order   int `gorm:"primaryKey"`
}

/* returns the labels for an item, preserving their specified order in database
 * we JOIN the labels and item_labels table, filter by ItemID,
 * and apply custom sorting using the "order" field from item_labels */
func (item *Item) labelsFill() error {
	return db.Joins("JOIN item_labels on labels.id = item_labels.label_id").
		Where("item_id = ?", item.ID).
		Order("item_labels.`order` DESC").
		// Select is required so that Find only has one "order" column available,
		// otherwise, it will try to use the one from "Labels" which is not in the db schema
		Select("labels.id", "labels.name", "item_labels.`order`").
		Find(&item.Labels).
		Error
}

func itemLabelsFillAll(items *[]Item) (err error) {
	for i := range *items {
		if err = (*items)[i].labelsFill(); err != nil {
			return
		}
	}
	return
}

// ToDo: Finish for actually updating existing item's labels
func (new *Item) LabelsSync() (err error) {
	existing := Item{ID: new.ID}
	// TODO: If (due to previous corruption) an ItemLabel relation with label_id = 0 is in the database,
	// it won't be returned by the method below, leading to further issues in this function
	// this should never happen (and is a cause of other bugs that needs fixing),
	// but it is probably better to make the code below handle this case gracefully nevertheless
	if err = existing.labelsFill(); err != nil {
		return err
	}
	pointerExisting, pointerNew := len(existing.Labels)-1, len(new.Labels)-1 // pointer becomes -1 if empty slice
	if pointerNew < 0 {
		return fmt.Errorf("Cannot save no items in ItemLabelsSync")
	}

	tx := db.Begin()

	for pointerExisting >= 0 && existing.Labels[pointerExisting].Order < new.Labels[pointerNew].Order {
		// delete if there have been more labels with lower priority before
		tx.Delete(&ItemLabels{
			ItemID: new.ID,
			Label:  existing.Labels[pointerExisting],
			Order:  existing.Labels[pointerExisting].Order,
		})
		pointerExisting--
	}
	for pointerExisting >= 0 && pointerNew >= 0 && existing.Labels[pointerExisting].Order == new.Labels[pointerNew].Order {
		// Update the record if order matches but Label does not
		err = tx.Model(&ItemLabels{
			ItemID: new.ID,
			Order:  existing.Labels[pointerExisting].Order,
		}).Update("label_id", new.Labels[pointerNew].ID).Error
		if err != nil {
			return err
		}
		pointerExisting--
		pointerNew--
	}
	// once this is reached, we either no longer have existing or new labels
	for pointerExisting >= 0 {
		tx.Delete(&ItemLabels{
			ItemID: new.ID,
			Label:  existing.Labels[pointerExisting],
			Order:  existing.Labels[pointerExisting].Order,
		})
		pointerExisting--
	}
	for pointerNew >= 0 {
		tx.Omit("Label.Order").Create(&ItemLabels{
			ItemID: new.ID,
			Label:  new.Labels[pointerNew],
			Order:  new.Labels[pointerNew].Order,
		})
		pointerNew--
	}

	return tx.Commit().Error
}

func ItemLabelsFromString(labelNames string) (labels []Label) {
	labelTypes := strings.SplitN(labelNames, ";", 2)
	labels = itemLabelsConvert(strings.Split(labelTypes[0], ","), 1)
	if len(labelTypes) > 1 {
		labels = append(
			labels,
			itemLabelsConvert(strings.Split(labelTypes[1], ","), -1)...,
		)
	}
	return
}

func itemLabelsConvert(labelNames []string, orderType int) (labels []Label) {
	start := len(labelNames)
	if orderType == -1 {
		start = -1
	}
	for i, label := range labelNames {
		labels = append(labels, Label{
			Name:  strings.TrimSpace(label),
			Order: start - i,
		})
		if errors.Is(db.Where("name = ?", labels[i].Name).First(&labels[i]).Error, gorm.ErrRecordNotFound) {
			if err := db.Omit("Order").Create(&labels[i]).Error; err != nil {
				// ToDo
			}
		}
	}
	return
}

func itemLabelsToString(labels []Label) (str string) {
	separated := false
	first := true
	for _, label := range labels {
		if !separated && label.Order < 0 {
			separated = true
			str += "; " + label.Name
			continue
		}
		if first {
			first = false
		} else {
			str += ", "
		}
		str += label.Name
	}
	return
}

// delete labels that are no longer assigned
func itemLabelsCleanup() {
	_, err := dbx.Exec("DELETE FROM labels AS l WHERE NOT EXISTS " +
		"(SELECT i.id FROM item_labels AS il, items AS i WHERE il.label_id = l.id AND il.item_id=i.id);")
	core.LogErr(err)
}
