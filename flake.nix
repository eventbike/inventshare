# SPDX-FileCopyrightText: 2023 eventbike_zero Team (Verein zur Förderung innovativer Wohn- und Lebensformen e.V.) <team@eventbikezero.de>
# SPDX-License-Identifier: AGPL-3.0-or-later
{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs = {self, nixpkgs, flake-utils}: 
    flake-utils.lib.eachDefaultSystem
      (system:
        let
          pkgs = nixpkgs.legacyPackages.${system};
        in with pkgs; {
          devShells.default = mkShell {
            buildInputs = [ go gofumpt sqlite gopls delve reuse ];
          };
        }
    );
}
