// SPDX-FileCopyrightText: 2024 eventbike_zero Team (Verein zur Förderung innovativer Wohn- und Lebensformen e.V.) <team@eventbikezero.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package testing

import (
	"fmt"
	"testing"

	"eventbikezero.de/inventshare/assets"
)

func Translate(t *testing.T, key string, args ...interface{}) (msg string) {
	msg = assets.I18n.Tr("en", key, args)
	if msg == "" {
		t.Error(fmt.Errorf("Translation: Key %s not found in default.", key))
	}
	return
}
