# inventshare

Manage your things with a focus on **sharing**!

## Goals and non-goals

What we aim for:

- simple intuitive user experience
- easy to set up and host yourself (single binary + sqlite)
- managing items and rentals for things like clubs / NGOs, event technology and similar use cases
- share your stuff with ease
- integrate with other useful tools
- (ideally) federate: We are looking for collaboration to turn this into a federated software

What we are currently not interested in:

- managing stocks in a warehouse style (Free Software already exists for this task). We mostly track individual items.
- sharing single items (have a look at Commons Booking if you want to share your screwdriver or cargo bike)
- selling items (have a look at [flohmarkt](https://codeberg.org/flohmarkt/flohmarkt) instead)


## What we tried

While complicated asset management systems exist, they are often too overkill and hard to use. We do not need to store large quantities of items, we do not need to track many purchases, we do not need FIFO queues etc.  
In contrary, there are some systems for community-maintained stuff, but they are hard to scale to large quantities. You can't easily keep track of many items and their properties, you can't book 50 things for an event at once, they are more focused to "let's share that one bike and that one tool in the neighbourhood".

## What we are planning here

In 2022 we started writing a simple PHP tool that can manage a simple list of items,
and adding labels to it.

Our next goal is writing a more complete inventory management solution
that allows multiple users and teams to work on shared inventory lists.
It should be lightweight to use for hobbyists and powerful enough to manage large amounts of inventory with it.

In the future, the inventory should be shared between instances (like E-Mail works),
so you can share some items with another team by sharing it to project-xy@example.com,
or request event@event-company-xy.invalid to rent some items from them.

We are focusing events at first (that is with scheduling the uses and maybe having some special features like managing mandatory electrical safety checks).
However, we want to keep it usable as a generic tools for associations and everyone else who frequently shares material with others and needs a tool for that.
So we are looking for your feedback for other use-cases – tell us what you need!


## Contributing

Please have a look at our [dedicated contributing resources](./CONTRIBUTING/README.md).
We appreciate your input and support.

Thank you for reading through!

## License

inventshare is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

inventshare is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with inventshare. If not, see <https://www.gnu.org/licenses/>.
