# Templates

Docs: see https://pkg.go.dev/html/template

Some more project specific conventions are listed below.

## Naming and placement

- templates are separated by folders by their use in the project
- common templates that are always parsed are stored in partials/
- common templates start with an upper-case letter (just as exported functions in Go)
- local templates always start with a lower case letter
- template names should be as clear as possible, e.g. listing items should be "items" rather than "list", see 260e1204b3168949509ea92163ae91d3a5416c51
