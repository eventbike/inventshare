// SPDX-FileCopyrightText: 2023 eventbike_zero Team (Verein zur Förderung innovativer Wohn- und Lebensformen e.V.) <team@eventbikezero.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package data

import (
	"gorm.io/gorm"
)

type AdhocCustomer struct {
	gorm.Model
	Name     string
	Address  string
	Contact  string
	IdCardID int
	IdCard   Attachment
}

type AdhocRental struct {
	gorm.Model
	AdhocCustomerID uint
	AdhocCustomer   AdhocCustomer
	ProjectID       uint
	ItemID          uint
	Item            Item
}

type AdhocProject struct {
	Project
	ItemsAvailable []Item
	ItemsLent      []Item
}

func AdhocCustomerNew(customer *AdhocCustomer) error {
	result := db.Create(&customer)
	if result.Error != nil {
		return result.Error
	}
	return nil
}

func AdhocRentalNew(rental *AdhocRental) error {
	result := db.Create(&rental)
	if result.Error != nil {
		return result.Error
	}
	return nil
}

func AdhocRentalReturn(rental *AdhocRental) error {
	db.Where(&rental).Delete(&rental)
	return nil
}

func AdhocRentalGet(itemID uint, projectID uint) (AdhocRental, error) {
	var adhocRental AdhocRental
	result := db.Where("item_id = ? AND project_id = ?", itemID, projectID).Preload("Item").Preload("AdhocCustomer.IdCard").Last(&adhocRental)
	if result.Error != nil {
		return adhocRental, result.Error
	}
	return adhocRental, nil
}

func AdhocProjectGet(projectID uint) (AdhocProject, error) {
	var adhocProject AdhocProject
	db.First(&adhocProject.Project, projectID)
	subqueryRentals := db.Table("adhoc_rentals").Where("project_id = ? AND deleted_at IS NULL", projectID).Select("item_id")
	// db.Debug().Model(&Item{}).Preload("Labels").Joins("join adhoc_rentals on item_id = items.id").Where("project_id = ?", projectID).Scan(&adhocProject.ItemsLent)
	db.Not("id IN (?)", subqueryRentals).Where("id IN (?)", db.Table("project_items").Where("project_id = ?", projectID).Select("item_id")).Preload("Labels").Find(&adhocProject.ItemsAvailable)
	db.Where("id IN (?)", subqueryRentals).Preload("Labels").Find(&adhocProject.ItemsLent)
	return adhocProject, nil
}
