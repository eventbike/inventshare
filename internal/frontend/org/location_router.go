// SPDX-FileCopyrightText: 2024 eventbike_zero Team (Verein zur Förderung innovativer Wohn- und Lebensformen e.V.) <team@eventbikezero.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package org

import (
	"net/http"

	"eventbikezero.de/inventshare/internal/data"
	"eventbikezero.de/inventshare/internal/frontend/util"
	"github.com/go-chi/chi/v5"
)

func locationRouter(router chi.Router) {
	router.Get("/new", util.ExecFrontend(locationNewForm, "org/location/form"))
	router.Post("/new", util.Exec(locationSavePost))
	router.Post("/htmx/newPost", util.ExecFrontend(htmxNewLocationPost, "partials/select/location"))
	router.Route("/{locationID}-{locationSlug}", func(subrouter chi.Router) {
		subrouter.Get("/", util.ExecFrontend(locationItems, "org/location/items"))
		subrouter.Get("/settings", util.ExecFrontend(locationSettingsForm, "org/location/form"))
		subrouter.Post("/settings", util.Exec(locationSavePost))
		subrouter.Post("/delete", util.Exec(locationDeletePost))
	})
}

func locationIndex(r util.Request) error {
	// context
	org, err := util.OrgFromRequest(r.Req)
	if err != nil {
		return err
	}
	r.Data["Org"] = &org

	r.Data["Locations"], err = org.Locations()
	return err
}

func locationNewForm(r util.Request) error {
	org, err := util.OrgFromRequest(r.Req)
	r.Data["Org"] = &org
	r.Data["Location"] = &data.Location{Org: org}

	return err
}

func locationSettingsForm(r util.Request) error {
	org, err := util.OrgFromRequest(r.Req)
	if err != nil {
		return err
	}
	location, err := util.LocationFromRequest(r.Req)
	r.Data["Org"] = &org
	r.Data["Location"] = &location

	return err
}

func locationSavePost(r util.Request) error {
	location, err := util.LocationFromPost(r.Req)
	if err != nil {
		return err
	}
	r.Redirect(location.Path(), http.StatusSeeOther)
	return nil
}

func htmxNewLocationPost(r util.Request) error {
	location, err := util.LocationFromPost(r.Req)
	if err != nil {
		return err
	}

	r.Data["SelectedLocation"] = location.ID
	r.Data["CreatedLocation"] = true

	locations, err := location.Org.Locations()
	if err != nil {
		return err
	}
	r.Data["AvailableLocations"] = append(locations, location)

	r.Resp.Header().Add("Inv-Location-Id", util.StringFromUint(location.ID))
	return nil
}

func locationDeletePost(r util.Request) error {
	location := data.Location{
		ID: util.UintFromString(r.Req.PostFormValue("locationID")),
	}
	if err := location.Lookup(); err != nil {
		return err
	}
	org := location.Org
	if err := location.Delete(); err != nil {
		return err
	}
	r.Redirect(org.Path()+"/locations", http.StatusSeeOther)
	return nil
}

func locationItems(r util.Request) (err error) {
	r.Data["RenderItems"] = util.RenderItems

	// context
	location, err := util.LocationFromRequest(r.Req)
	if err != nil {
		return
	}
	r.Data["Location"] = &location
	r.Data["Org"] = &location.Org

	r.Data["Items"], err = location.Items()
	return
}
