// SPDX-FileCopyrightText: 2023 eventbike_zero Team (Verein zur Förderung innovativer Wohn- und Lebensformen e.V.) <team@eventbikezero.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package data

import (
	"testing"
)

func testStringMustEqual(t *testing.T, got, want string) {
	if got != want {
		t.Errorf("\nhave: %+v\nwant: %+v", got, want)
	}
}

func TestCreateSlug(t *testing.T) {
	testStringMustEqual(t,
		createSlug("events-are-great"),
		"events-are-great")
	testStringMustEqual(t,
		createSlug("Sweet-Dreams"),
		"sweet-dreams")
	testStringMustEqual(t,
		createSlug("hi"),
		"hi")
	testStringMustEqual(t,
		createSlug("Bullshit"),
		"bullshit")
	testStringMustEqual(t,
		createSlug("Let's store something! "),
		"let-s-store-something")
	testStringMustEqual(t,
		createSlug("Blöde Straße 31b"),
		"bloede-strasse-31b")
	testStringMustEqual(t,
		createSlug("🚀 I missed the description field and want to put a ROMAN² into the „Title“ field! ;-)"),
		"i-missed-the-description-field-and-want-to-put-a-roman-into-the-title-field")
}
