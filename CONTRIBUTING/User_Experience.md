# User Experience

Below, we try to collect some UX design strategies we follow.
We priorize user experience over look of the application.
Our guidelines are not set in stone
and you are free to start a discussion about it.

However, we aim to create a **consistent experience**
based on these guidelines.
If you notice a violation in the application,
please report it and it will be considered a bug.


## Empty state

Whenever there is an empty state (e.g. no Items yet),
a hint should explain how to get something there.

Later, when filters are implemented,
the hint should also suggest to modify filters if applicable.


## Modals

We use modals to allow quickly adding missing things
without leaving the page.
They aim not to disrupt the user in whatever they do,
but help them quickly access useful external content or settings,
saving them the need to navigate to a separate page.

Modals should

- always be initiated by user action (e.g. clicking a button)
- never open automatically
  (users tend to intuitively dismiss modals they didn't expect without reading them first)
- preserve the background activity, which implies saving the state in case a full page reload is required or submitting data with JS


## Error messages

Error handling on a technical level is still to be determined.
Our goal is to inform the user about the state by tracking what has been done.
Even when "unknown" errors occured,
we should be able to tell until which point their action was performed.

Error messages should be actionable to the user:
They contain an identifier and instructions on how to report the issue with necessary details.
