// SPDX-FileCopyrightText: 2023 eventbike_zero Team (Verein zur Förderung innovativer Wohn- und Lebensformen e.V.) <team@eventbikezero.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package util

import (
	"strconv"
)

func StringFromUint(n uint) string {
	return strconv.FormatUint(uint64(n), 10)
}

func UintFromString(str string) uint {
	n, err := strconv.ParseUint(str, 10, 64)
	if err != nil {
		return 0
	}
	return uint(n)
}
