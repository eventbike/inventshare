// SPDX-FileCopyrightText: 2023 eventbike_zero Team (Verein zur Förderung innovativer Wohn- und Lebensformen e.V.) <team@eventbikezero.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package testing

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"testing"

	"eventbikezero.de/inventshare/internal/core"
)

type Response struct {
	HTTP *http.Response
	Body []byte
	T    *testing.T
}

type Fields url.Values

type Form struct {
	URL    string
	Action string
	Fields Fields
	T      *testing.T
}

type Object struct {
	Path string
	ID   string
}

// create global http.Client for specific properties ...
var client http.Client

var dummyCounter = 0

func dummyNext() string {
	dummyCounter++
	return strconv.Itoa(dummyCounter) + "_"
}

// ... like returning redirects literally instead of following them,
// so we can get the explicit URL for reuse and check if the redirect status is correct
func init() {
	client = http.Client{
		CheckRedirect: func(_ *http.Request, _ []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}
}

func StringsMustEqual(t *testing.T, a, b string) {
	if a != b {
		t.Error(fmt.Errorf("'%s' != '%s'", a, b))
	}
}

func StringsMustDiffer(t *testing.T, a, b string) {
	if a == b {
		t.Error(fmt.Errorf("'%s' == '%s'", a, b))
	}
}

func Get(t *testing.T, path string) *Response {
	var err error
	response := &Response{T: t}
	response.HTTP, err = client.Get("http://" + bindString + path)
	if err != nil {
		t.Error(err)
	}
	return response.MustNotContain("<h1>Error</h1>")
}

func Post(t *testing.T, path string, fields Fields) (response *Response) {
	var err error
	response = &Response{T: t}
	response.HTTP, err = client.PostForm("http://"+bindString+path, url.Values(fields))
	if err != nil {
		t.Error(err)
	}
	return
}

// automatically format an error message with request method and URL
func (r *Response) Error(message string) error {
	return fmt.Errorf("'%s %s': %s\noccured in: \n%s", r.HTTP.Request.Method, r.HTTP.Request.URL.Path, message, core.GetCallStack())
}

// reads the body as bytes
func (r *Response) readBody() {
	if r.Body != nil { // body apparently read already
		return
	}
	defer r.HTTP.Body.Close()
	var err error
	r.Body, err = io.ReadAll(r.HTTP.Body)
	if err != nil {
		r.T.Error(err)
	}
}

// save Body as file
func (r *Response) dumpBody() {
	if err := os.WriteFile("failed.html", r.Body, 0o600); err != nil {
		panic("Writing file failed: " + err.Error())
	}
	fmt.Println("Output dumped to failed.html")
}

// FORMS

func (f *Form) Test() *Response {
	FormPage := Get(f.T, f.URL).
		MustStatusOK().
		MustContain("</form>")

	if !FormPage.containsAny("<form action=\""+f.Action+"\"", "hx-post=\""+f.Action+"\"") {
		_ = FormPage.MustContain("<form action=\"\"")
		StringsMustEqual(f.T, f.URL, f.Action)
	}

	// check that all sent fields are actually present in the HTML
	for key := range f.Fields {
		_ = FormPage.MustContain(" name=\"" + key + "\" ")
	}

	return Post(f.T, f.Action, f.Fields)
}

// STATUS

func (r *Response) MustStatus(code int) *Response {
	if r.HTTP.StatusCode != code {
		r.T.Error(r.Error("Status code did not match, expected " + fmt.Sprint(code) + ", got " + fmt.Sprint(r.HTTP.StatusCode)))
	}
	return r
}

func (r *Response) MustStatusOK() *Response {
	return r.MustStatus(200).
		MustNotContain("Error Code").
		MustNotContain("Not Found")
}

func (r *Response) MustStatusSeeOther() *Response {
	return r.MustStatus(303).MustEmpty()
}

func (r *Response) MustStatusNotFound() *Response {
	return r.MustStatus(404).
		MustNotContain("Error Code").
		MustContain("Not Found")
}

// EMPTY

func (r *Response) isEmpty() bool {
	r.readBody()
	if len(r.Body) == 0 {
		return true
	} else {
		return false
	}
}

func (r *Response) MustEmpty() *Response {
	if !r.isEmpty() {
		r.dumpBody()
		r.T.Error(r.Error("should be empty, but is not"))
	}
	return r
}

func (r *Response) MustNotEmpty() *Response {
	if r.isEmpty() {
		r.T.Error(r.Error("should not be empty, but is"))
	}
	return r
}

// CONTAINS

func (r *Response) contains(content string) bool {
	r.readBody()
	if len(r.Body) == 0 {
		return false
	}
	return bytes.Contains(r.Body, []byte(content))
}

func (r *Response) containsAny(contents ...string) bool {
	for _, content := range contents {
		if r.contains(content) {
			return true
		}
	}
	// if loop ends, nothing was found, and we error
	return false
}

func (r *Response) MustContain(contents ...string) *Response {
	for _, content := range contents {
		if !r.contains(content) {
			r.dumpBody()
			r.T.Error(r.Error("should contain [" + content + "], but does not"))
		}
	}
	return r
}

func (r *Response) MustContainAny(contents ...string) *Response {
	if !r.containsAny(contents...) {
		r.dumpBody()
		r.T.Error(r.Error("should contain any of [" + fmt.Sprintf("%+v", contents) + "], but does not"))
	}
	return r
}

func (r *Response) MustNotContain(contents ...string) *Response {
	if r.containsAny(contents...) {
		r.dumpBody()
		r.T.Error(r.Error("should not contain [" + fmt.Sprintf("%+v", contents) + "], but does"))
	}
	return r
}
