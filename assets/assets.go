// SPDX-FileCopyrightText: 2023 eventbike_zero Team (Verein zur Förderung innovativer Wohn- und Lebensformen e.V.) <team@eventbikezero.de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package assets

import (
	"embed"

	"github.com/kataras/i18n"
)

//go:embed locales/*
var locales embed.FS
var I18n *i18n.I18n

//go:embed css/* js/*
var Static embed.FS

func init() {
	loader, err := i18n.FS(locales, "./locales/*/*.yaml")
	if err != nil {
		panic(err.Error())
	}
	I18n, err = i18n.New(loader)
	if err != nil {
		panic(err.Error())
	}
	/* This is discouraged as per official documentation,
	 * but we want to automatically load all languages (and not specify a custom order),
	 * and when auto-loading, `da` becomes the default.
	 * There is probably a better solution,
	 * but it's also not obvious why use of this function is discouraged and it seems to work™ */
	I18n.SetDefault("en")
}
